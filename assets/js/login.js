$(function() {
    $('.login-box a').on('click', function() {
        $('.login-box').hide()
        $('.reg-box').show()
    })
    $('.reg-box a').on('click', function() {
        $('.login-box').show()
        $('.reg-box').hide()
    })
    
    layui.form.verify ({
        passwordLen: [/^[\S]{6,16}$/,'密码为6-16位非空格字符'],
        confirmPass(value) {
            if($('.reg-box [name=password]').val() !==value)
            return '两次密码不一致'
        },        
    })
}) 
   
    $('#form_reg').on('submit',function(event) {
        event.preventDefault()
        const index= layui.layer.load()
        $.ajax({
            type:'post',
            url:`/api/reguser`,
            data:$(this).serialize(),
            success({status,message}) {
                layui.layer.close(index)
                if(status===0) {
                    layui.layer.msg(' 注册成功')
                    $('.reg-box .link-btn').trigger('click')
                }else{
                   layui.layer.msg(message)
                }
            }
        })
    })
    $('#form_login').on('submit',function(event) {
        event.preventDefault()
        const index= layui.layer.load()
        $.ajax({
            type:'post',
            url:`/api/login`,
            data:$(this).serialize(),
            success({status,message,token}) {
                layui.layer.close(index)
                if(status===0) {
                    layui.layer.msg(' 登录成功')
                    localStorage.setItem('token',token)
                    window.location.href='/index.html'
                }else{
                   layui.layer.msg(message)
                }
            }
        })
    })


