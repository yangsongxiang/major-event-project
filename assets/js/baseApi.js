const Baseurl = 'http://api-breakingnews-web.itheima.net'
$.ajaxPrefilter((options) => {
    // 以/my 开头的需要权限 要带上token 否则不带
    //在最前面写意味着还没拼接url
    //startsWith 判断第一个字母是什么开头
  
    if (options.url.startsWith('/my')) {
        //直接给headers的内容赋值
        options.headers = {
            Authorization: localStorage.getItem('token')
        }
        options.complete = ({
            responseJSON
        }) => {
            if (responseJSON.status === 1 &&
                 responseJSON.message === '身份认证失败！') 
                 {
                localStorage.removeItem('token')
                window.location.href = '/login.html'
            }
        }
    }
    options.url = `${Baseurl}${options.url}` // 不需要写return
    //这里写 没有return 没有返回值
    // 登录成功 有可能这个函数的返回值 没有执行结束
})