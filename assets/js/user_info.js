
layui.form.verify({
    // 自定义规则
    nickLen: [/^[\S]{1,6}$/, "昵称的长度1-6字符之间"],
  })
  
function getUserInfo() {
    
    $.ajax({
        url: '/my/userinfo',
        success({
            status,
            message,
            data
        }) {
            if (status === 0) {
                //表示获取数据成功
                //data=>form表单
                
                layui.form.val('formData', data) //设置表单数据
            } else {
                layui.layer.msg(message)
            }
        },
    })
}

//注册事件
function registerEvent() {
    $('#btnReset').on('click', function (event) {
        
        event.preventDefault();
        getUserInfo() //重新加载数据
    });
    $('#myForm').on('submit', function (event) {
        event.preventDefault() //组织表单默认事件
        //获取表单数据
        $.ajax({
            type: 'post',
            url: '/my/userinfo',
            data: layui.form.val('formData'),
            success({
                status,
                message
            }) {
                layui.layer.msg(status ? '修改用户资料成功' : message)
            },
        })
    })
}
getUserInfo()
registerEvent()