layui.form.verify({
    // 自定义规则
    passwordLen: [/^[\S]{1,6}$/, "昵称的长度1-6字符之间"],
    confirmPass(value) {
        if($('input[name=newPwd]').val() !==value)
        return '两次密码不一致'
    },        
  })
  $('#myForm').on('submit', function (event) {
    event.preventDefault() //组织表单默认事件
    $.ajax({
        type: 'post',
        url: '/my/updatepwd',
        data: layui.form.val('formData'),
        success({
            status,
            message
        }) {
            if (status===0) {
                layui.layer.msg('修改密码成功')
            } else{
                layui.layer.msg(message)
            }
        }
    })
})
  